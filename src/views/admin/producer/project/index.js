/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";
import { useAuthContext } from "../../../../hooks/useAuthContext";
import { useProjectContext } from "../../../../hooks/useProjectContext";
// Chakra imports
import { Box, Flex, useColorModeValue, Button, Text } from "@chakra-ui/react";

// Custom components
import Template from "layouts/producer/Default";
import ProjectSettings from "./components/Settings";
import FormCard from "./components/FormCard";

export default function Project(props) {
  let mainText = useColorModeValue("navy.700", "white");
  const [projectPath, setProjectPath] = useState(null);
  const [project, setProject] = useState();
  const { user } = useAuthContext();
  const { dispatch } = useProjectContext();
  const [route, setRoute] = useState("edit");
  const [inProgress, setInProgress] = useState(false);
  const renderScene = () => {
    switch (route) {
      case "settings":
        return (
          <ProjectSettings
            project={project}
            setProject={setProject}
            user={user}
            project_path={projectPath}
          />
        );
      case "edit":
        return <FormCard project={project} setProject={setProject} />;
      default:
        return null;
    }
  };

  useEffect(() => {
    const fetchProject = async () => {
      if (inProgress) return;
      if (!user) return;
      setInProgress(true);
      const project_id = props.match.params.id;
      let project_path =
        process.env.REACT_APP_API_URL + "/projects/" + project_id;
      setProjectPath(project_path);
      const response = await fetch(project_path, {
        method: "GET",
        headers: {
          "Access-Control-Allow-Credentials": true,
          "Content-Type": "application/json",
          Authorization: user,
        },
      });
      const json = await response.json();
      setInProgress(false);
      if (response.ok) {
        setProject(json.project);
        dispatch({ type: "LOGIN", payload: json.project });
        localStorage.setItem("project_id", json.project.id);
        console.log("GET PROJECT RES: ", json.project);
      }
    };

    if (user) {
      fetchProject();
    }
  }, [user]);

  // Chakra Color Mode
  return (
    <Template>
      {project && (
        <Flex
          w="100%"
          textAlign={"center"}
          justifyContent={"center"}
          alignItems={"center"}
          flexDirection="column"
        >
          <Flex
            w="90%"
            minW="160px"
            marginBottom={"10px"}
            justifyContent={"center"}
          >
            <Text color={mainText} fontWeight="bold" fontSize="34px" mb="30px">
              {project.name}
            </Text>
            <Button
              ml="20px"
              mt="5px"
              variant={"action"}
              fontSize="sm"
              fontWeight="500"
              borderRadius="70px"
              px="24px"
              py="5px"
            >
              {project.status}
            </Button>
          </Flex>

          <Flex mb="20px" flexWrap={"wrap"}>
            <Button
              m="10px"
              onClick={() => setRoute("edit")}
              variant={route == "edit" ? "light" : "outline"}
              fontSize="sm"
              fontWeight="500"
              borderRadius="70px"
              px="24px"
              py="5px"
            >
              Project Details
            </Button>
            <Button
              m="10px"
              onClick={() => setRoute("settings")}
              variant={route == "settings" ? "light" : "outline"}
              fontSize="sm"
              fontWeight="500"
              borderRadius="70px"
              px="24px"
              py="5px"
            >
              Project Settings
            </Button>
          </Flex>
          {renderScene()}
        </Flex>
      )}
    </Template>
  );
}
