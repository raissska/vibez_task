// Chakra imports
import { Icon, Flex, Avatar, Text, useColorModeValue } from "@chakra-ui/react";
// Assets
import React, { useState, useEffect } from "react";
// Custom components
import PersonPopup from "components/popups/PersonPopup";

export function PersonCell(props) {
  const { user, person, membership } = props;
  const [showDetails, setShowDetails] = useState(false);
  const textColor = useColorModeValue("secondaryGray.900", "white");
  const toggleClosure = () => {
    setShowDetails(!showDetails);
  };

  const renderPersonPopup = () => {
    if (showDetails)
      return (
        <PersonPopup
          user={user}
          person_id={person.id}
          toggleClosure={toggleClosure}
        />
      );
  };

  useEffect(() => {
    console.log("OPEN: ", showDetails);
  }, [showDetails]);

  return (
    <Flex
      // onMouseEnter={() => {
      //   setShowDetails(true);
      // }}
      // onMouseLeave={() => {
      //   setShowDetails(false);
      // }}
      onClick={() => toggleClosure()}
    >
      {renderPersonPopup()}
      <Flex direction={"column"} justifyContent={"center"}>
        <Avatar h="42px" w="42px" src={person?.avatar} me="14px" />
      </Flex>

      <Flex direction={"column"} justifyContent={"center"}>
        <Text me="10px" color={textColor} fontSize="sm" fontWeight="700">
          {person?.name}
        </Text>
        <Text me="10px" color={textColor} fontSize="sm" fontWeight="700">
          {membership?.status == "pending"
            ? "Pending"
            : membership?.name
            ? membership?.name
            : membership?.membership}
        </Text>
      </Flex>
    </Flex>
  );
}
