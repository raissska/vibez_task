// Chakra imports
import {
  Avatar,
  Button,
  Flex,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import {
  renderThumb,
  renderTrack,
  renderView,
} from "components/scrollbar/Scrollbar";
import { Scrollbars } from "react-custom-scrollbars-2";
// Custom icons
import React, { useState, useEffect } from "react";
import { HSeparator } from "components/separator/Separator";
import { normalizeDate } from "utils/datetime";
import { Social } from "components/social/social";
import { calculateAge } from "utils/datetime";

export default function PersonPopup(props) {
  const { user, person_id, toggleClosure } = props;
  let mainText = useColorModeValue("navy.700", "white");
  const [error, setError] = useState(null);
  const [person, setPerson] = useState(null);
  const [inProgress, setInProgress] = useState(false);
  const hash = window.location.hash;

  const fetchPerson = async () => {
    setError("");
    if (inProgress) return;
    if (!user) {
      setError("You not authorized. Try login again.");
      return;
    }
    setInProgress(true);
    let project_id = localStorage.getItem("project_id");
    let path =
      process.env.REACT_APP_API_URL +
      "/projects/" +
      project_id +
      "/people/" +
      person_id;
    console.log("PERSON PATH: ", path);
    const response = await fetch(path, {
      method: "GET",
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: user,
      },
    });
    const json = await response.json();
    console.log("GET PERSON RES: ", json);

    if (response.ok) {
      setPerson(json.person);
    } else {
      setError(response?.error?.message);
    }
    setInProgress(false);
  };

  useEffect(() => {
    if (user) {
      fetchPerson();
    }
  }, [user]);

  return (
    <Flex
      w="100%"
      h="100%"
      position={"fixed"}
      right="0px"
      top="0px"
      zIndex={999999}
    >
      <Flex flex={1}></Flex>
      <Flex
        width="30px"
        bg="linear-gradient(to right, white , lightgrey)"
      ></Flex>
      <Flex
        width="40%"
        minW="390px"
        bgColor="white"
        borderLeftWidth={1}
        borderLeftColor="2A2A2A"
        direction={"column"}
        padding={{ base: "20px", md: "20px", lg: "40px", xl: "40px" }}
      >
        <Flex id="popup_top" mb="20px">
          <Button onClick={() => toggleClosure()} w="20px" h="20px">
            X
          </Button>
        </Flex>
        {error && <Text color="red">{error}</Text>}
        {person && (
          <Flex
            id="popup_body"
            h="100%"
            // justifyContent={"center"}
            borderWidth={{ base: 0, md: 1, lg: 1, xl: 1 }}
            borderColor={"lightgrey"}
            borderRadius={"20px"}
            paddingTop={"30px"}
            minW="350px"
            flex={1}
            justifyContent={"space-around"}
          >
            <Flex
              h="100%"
              direction={"column"}
              w={{ base: "90%", md: "80%", lg: "80%", xl: "80%" }}
            >
              <Scrollbars
                autoHide
                renderTrackVertical={renderTrack}
                renderThumbVertical={renderThumb}
                renderView={renderView}
              >
                <Flex direction={"column"}>
                  <Flex
                    direction={"column"}
                    justifyContent={"center"}
                    alignItems={"center"}
                    mb="30px"
                  >
                    <Avatar
                      h="100px"
                      w="100px"
                      mb="10px"
                      src={person?.avatar}
                    />
                    <Text fontSize={"md"} fontWeight="500" color={mainText}>
                      {person.name}
                    </Text>
                  </Flex>
                  <Flex mt="8px">
                    <Text color={mainText}>Member status: </Text>
                    <Text ml="16px" color={mainText}>
                      {person?.membership?.status}
                    </Text>
                  </Flex>
                  <Flex mt="8px">
                    <Text color={mainText}>Since: </Text>
                    <Text ml="16px" color={mainText}>
                      {normalizeDate(person?.membership?.since)}
                    </Text>
                  </Flex>
                  <HSeparator mb="20px" mt="20px" />
                  {person?.membership?.status == "active" && (
                    <>
                      <Flex>
                        <Text color={mainText}>Member type: </Text>
                        <Text ml="16px" color={mainText}>
                          {person?.membership?.name}
                        </Text>
                      </Flex>
                      <HSeparator mb="20px" mt="20px" />
                    </>
                  )}
                  <Flex mt="8px">
                    <Text color={mainText}>Gender: </Text>
                    <Text ml="16px" color={mainText}>
                      {person.gender}
                    </Text>
                  </Flex>
                  <Flex mt="8px">
                    <Text color={mainText}>Date of birth: </Text>
                    <Text ml="16px" color={mainText}>
                      {normalizeDate(person.dob)}
                    </Text>
                    <Text ml="24px" color={mainText}>
                      Age:{" "}
                    </Text>
                    <Text ml="16px" color={mainText}>
                      {calculateAge(person.dob)}
                    </Text>
                  </Flex>
                  <Flex mt="8px">
                    <Text color={mainText}>Events participated:</Text>
                    <Text ml="16px" color={mainText}>
                      {person?.events_participated}
                    </Text>
                  </Flex>
                  <Flex mt="8px">
                    <Text color={mainText}>Purchases: </Text>
                    <Text ml="16px" color={mainText}>
                      {person?.num_of_orders} orders, {person?.total_purchases}{" "}
                      ILS
                    </Text>
                  </Flex>
                  <Flex mt="8px">
                    <Text color={mainText}>Email: </Text>
                    <Text ml="16px" color={mainText}>
                      {person.email}
                    </Text>
                  </Flex>
                  <Flex mt="8px">
                    <Text color={mainText}>Phone: </Text>
                    <Text ml="16px" color={mainText}>
                      {person.phone}
                    </Text>
                  </Flex>
                  <Flex mt="8px" color={mainText}>
                    <Text mr="16px">Social links:</Text>
                    <Social person={person} />
                  </Flex>
                </Flex>
              </Scrollbars>
            </Flex>
          </Flex>
        )}
      </Flex>
    </Flex>
  );
}
